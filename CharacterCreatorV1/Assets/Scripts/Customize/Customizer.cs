﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Customizer : MonoBehaviour {

    public GameObject[] part;
    int partNum;

    //This function is for selecting the next part
    public void PartChangeUp()
    {
        //Runs when at the top of the array to go forwards through the array
        if (partNum == part.Length - 1)
        {
            part[partNum].SetActive(false);
            partNum = 0;
            part[partNum].SetActive(true);
        }

        //Runs when in the middle or bottom of the array to go forwards through the array
        else
        {
            part[partNum].SetActive(false);
            partNum += 1;
            part[partNum].SetActive(true);
        }
    }


    //This function is for selecting the previous part
    public void PartChangeDown()
    {
        //Runs when at the bottom of the array to go backwards through the array
        if (partNum == 0)
        {
            part[partNum].SetActive(false);
            partNum = part.Length - 1;
            part[partNum].SetActive(true);
        }

        //Runs when in the middle or top of the array to go backwards through the array
        else
        {
            part[partNum].SetActive(false);
            partNum -= 1;
            part[partNum].SetActive(true);
        } 
    }
}
